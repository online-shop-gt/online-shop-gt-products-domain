package xyz.microservicios.shopGt.products.api

import io.mockk.every
import io.mockk.mockk
import org.junit.Test
import xyz.microservicios.shopGt.products.domain.entities.Product
import xyz.microservicios.shopGt.products.domain.services.ProductService
import xyz.microservicios.shopGt.products.infrastructure.repository.ProductRepository
import xyz.microservicios.shopGt.products.presentation.ProductCreationOrUpdateRequest
import xyz.microservicios.shopGt.products.presentation.UserInputValuesException
import java.math.BigDecimal
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class ProductApiTest {

    private object ProductData {
        const val accountId = 14071992L
        const val productId = 52L
        const val productName = "HUEVOS"
        val productPrice: BigDecimal = BigDecimal(1.10)
    }

    @Test
    fun `crear nuevo producto`() {
        val productMock = Product(
            ProductData.accountId,
            ProductData.productName,
            ProductData.productPrice
        )
        val productRepository = mockk<ProductRepository>()

        every {
            productRepository.create(productMock)
        } answers {
            arg<Product>(0).id = ProductData.productId
        }

        val productApi = ProductApi(ProductService(productRepository))

        val request = ProductCreationOrUpdateRequest().apply {
            this.accountId = ProductData.accountId
            this.productName = ProductData.productName
            this.productDescription = "Huevo granja azul"
            this.productPrice = ProductData.productPrice
        }

        assertEquals(52L, productApi.createProduct(request))
    }

    @Test
    fun `crear nuevo producto sin datos requeridos debe retornar una excepcion`() {
        val productMock = Product(
            ProductData.accountId,
            ProductData.productName,
            ProductData.productPrice
        )
        val productRepository = mockk<ProductRepository>()

        every {
            productRepository.create(productMock)
        } answers {
            arg<Product>(0).id = ProductData.productId
        }

        val productApi = ProductApi(ProductService(productRepository))

        val request = ProductCreationOrUpdateRequest().apply {
            this.productName = ProductData.productName
            this.productDescription = "Huevo granja azul"
        }

        assertFailsWith<UserInputValuesException> { productApi.createProduct(request) }
    }

}
