package xyz.microservicios.shopGt.products.exceptions

class DomainEntityNotFoundException(message: String = "Entidad de dominio no encontrada") : Exception(message)
