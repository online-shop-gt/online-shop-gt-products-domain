package xyz.microservicios.shopGt.products.domain.services

import xyz.microservicios.shopGt.products.domain.entities.Product
import xyz.microservicios.shopGt.products.exceptions.DomainEntityNotFoundException
import xyz.microservicios.shopGt.products.infrastructure.repository.ProductRepository
import xyz.microservicios.shopGt.products.presentation.ProductCreationOrUpdateRequest

class ProductService(private val productRepository: ProductRepository) {

    fun createProduct(request: ProductCreationOrUpdateRequest): Long {
        val product = Product(request.accountId, request.productName, request.productPrice)

        productRepository.create(product)

        return product.id
    }

    fun updateProduct(request: ProductCreationOrUpdateRequest) {
        val product = productRepository.findById(request.productId) ?: throw DomainEntityNotFoundException()

        product.apply {
            this.productName = request.productName
            this.productPrice = request.productPrice
            this.productDescription = request.productDescription
        }

        productRepository.update(product)
    }

}
