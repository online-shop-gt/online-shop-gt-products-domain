package xyz.microservicios.shopGt.products.domain.entities

import java.math.BigDecimal

data class Product(
    var accountId: Long,
    var productName: String,
    var productPrice: BigDecimal
) {
    var id: Long = 0
    var productDescription: String = ""
}
