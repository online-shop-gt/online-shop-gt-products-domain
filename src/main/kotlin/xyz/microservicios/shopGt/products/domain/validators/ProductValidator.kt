package xyz.microservicios.shopGt.products.domain.validators

import xyz.microservicios.shopGt.products.presentation.ProductCreationOrUpdateRequest
import xyz.microservicios.shopGt.products.presentation.UserInputValueError
import xyz.microservicios.shopGt.products.presentation.UserInputValuesException

class ProductValidator(private val request: ProductCreationOrUpdateRequest) {
    private val errors = mutableListOf<UserInputValueError>()

    fun validateToUpdate() {
        validateBasicProductInfo()
        validateProductId()

        if (errors.isNotEmpty()) {
            throw UserInputValuesException(errors)
        }
    }

    fun validate() {
        validateBasicProductInfo()
        validateAccount()

        if (errors.isNotEmpty()) {
            throw UserInputValuesException(errors)
        }
    }

    private fun validateBasicProductInfo() {
        if (request.productName == "") {
            val description = "El nombre del producto es requerido"
            val error = UserInputValueError("01", "productName", description)
            errors.add(error)
        }

        if (request.productPrice.toDouble() <= 0) {
            val description = "El precio del producto debe ser mayor a cero."
            val error = UserInputValueError("02", "productPrice", description)
            errors.add(error)
        }
    }

    private fun validateAccount() {
        if (request.accountId <= 0L) {
            val description = "El producto debe estar asociado a una cuenta de usuario"
            val error = UserInputValueError("03", "accountId", description)
            errors.add(error)
        }
    }

    private fun validateProductId() {
        if (request.productId <= 0L) {
            val description = "El ID del producto es requerido para realizar la actualizacion"
            val error = UserInputValueError("04", "productId", description)
            errors.add(error)
        }
    }

}
