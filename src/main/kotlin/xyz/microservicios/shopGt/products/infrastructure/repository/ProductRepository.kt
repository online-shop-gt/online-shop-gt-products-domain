package xyz.microservicios.shopGt.products.infrastructure.repository

import xyz.microservicios.shopGt.products.domain.entities.Product

interface ProductRepository {

    fun create(product: Product)

    fun update(product: Product)

    fun findById(id: Long): Product?

}
