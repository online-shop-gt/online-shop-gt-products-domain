package xyz.microservicios.shopGt.products.infrastructure.services

interface EmailService {

    fun send(from: String, to: List<String>, body: String)

}
