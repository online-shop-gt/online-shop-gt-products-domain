package xyz.microservicios.shopGt.products.api

import xyz.microservicios.shopGt.products.domain.services.ProductService
import xyz.microservicios.shopGt.products.domain.validators.ProductValidator
import xyz.microservicios.shopGt.products.presentation.ProductCreationOrUpdateRequest

class ProductApi(private val productService: ProductService) {

    fun createProduct(request: ProductCreationOrUpdateRequest): Long {
        ProductValidator(request).validate()
        return productService.createProduct(request)
    }

    fun updateProduct(request: ProductCreationOrUpdateRequest) {
        ProductValidator(request).validateToUpdate()
        productService.updateProduct(request)
    }

    fun readProductsByAccountId(accountId: Long) {
        
    }

}
