package xyz.microservicios.shopGt.products.presentation

class UserInputValueError(var code: String, var field: String, var description: String)
