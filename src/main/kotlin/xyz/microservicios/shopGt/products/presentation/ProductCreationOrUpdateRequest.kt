package xyz.microservicios.shopGt.products.presentation

import java.math.BigDecimal

class ProductCreationOrUpdateRequest {
    var productId: Long = 0L
    var accountId: Long = 0L
    var productName: String = ""
    var productDescription: String = ""
    var productPrice: BigDecimal = BigDecimal.ZERO
}
