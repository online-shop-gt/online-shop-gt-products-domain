package xyz.microservicios.shopGt.products.presentation

class UserInputValuesException(val errors: List<UserInputValueError>) : Exception("Error en entradas de usuario")
